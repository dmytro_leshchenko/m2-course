<?php
namespace Training\Preference\Model;

/**
 * @api
 */
class Preference implements PreferenceInterface
{
    /**
     * Create new object instance
     *
     * @param string $type
     * @param array $arguments
     * @return mixed
     */
    public function create(){

    }

    /**
     * Retrieve cached object instance
     *
     * @param string $type
     * @return mixed
     */
    public function get(){

    }

}
