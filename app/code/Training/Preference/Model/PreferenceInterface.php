<?php
namespace Training\Preference\Model;

/**
 * @api
 */
interface PreferenceInterface
{
    public function create();

    public function get($type);

}
